#include "satext/units.h"
#include "satos/kernel.h"

int main() {
    satos::kernel::initialize(64_MHz, 100_Hz);
    satos::kernel::start();
}
