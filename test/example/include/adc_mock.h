#pragma once

#include <cstdint>
#include "gmock/gmock.h"

namespace peripherals {
struct adc_mock {
    MOCK_METHOD0(read, std::uint32_t());
};
} // namespace peripherals
