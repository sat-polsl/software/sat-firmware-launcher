#pragma once

#include <cstdint>

namespace devices {
template<typename gpio, typename adc>
class heater {
public:
    heater(gpio& g, adc& a) : gpio_{g}, adc_{a}, threshold_{0} {}

    void process() {
        auto adc_value = adc_.read();
        if (adc_value <= threshold_) {
            gpio_.on();
        } else {
            gpio_.off();
        }
    }

    void set_threshold(std::uint32_t val) { threshold_ = val; }

    std::uint32_t get_threshold() { return threshold_; }

private:
    gpio& gpio_;
    adc& adc_;
    std::uint32_t threshold_;
};
} // namespace devices
