#pragma once

#include "gmock/gmock.h"

namespace peripherals {
struct gpio_mock {
    MOCK_METHOD0(on, void());
    MOCK_METHOD0(off, void());
};
} // namespace peripherals
