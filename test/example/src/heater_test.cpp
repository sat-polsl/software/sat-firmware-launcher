#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "adc_mock.h"
#include "gpio_mock.h"
#include "heater.h"

namespace {
using heater_type = devices::heater<peripherals::gpio_mock, peripherals::adc_mock>;

struct heater_test : testing::Test {
    peripherals::gpio_mock gpio_mock_;
    peripherals::adc_mock adc_mock_;
};

TEST_F(heater_test, set_get_threshold) {
    std::uint32_t threshold = 500;
    heater_type heater(gpio_mock_, adc_mock_);
    heater.set_threshold(threshold);

    ASSERT_EQ(heater.get_threshold(), threshold);
}

TEST_F(heater_test, adc_read_higher_than_threshold) {
    std::uint32_t threshold = 500;
    EXPECT_CALL(adc_mock_, read()).WillOnce(testing::Return(600));
    EXPECT_CALL(gpio_mock_, off()).Times(1);
    heater_type heater(gpio_mock_, adc_mock_);
    heater.set_threshold(threshold);
    heater.process();
}

TEST_F(heater_test, adc_read_lower_than_threshold) {
    std::uint32_t threshold = 500;
    EXPECT_CALL(adc_mock_, read()).WillOnce(testing::Return(400));
    EXPECT_CALL(gpio_mock_, on()).Times(1);
    heater_type heater(gpio_mock_, adc_mock_);
    heater.set_threshold(threshold);
    heater.process();
}
} // namespace
