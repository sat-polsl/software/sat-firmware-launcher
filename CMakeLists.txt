cmake_minimum_required(VERSION 3.25)
set(CMAKE_CONFIGURATION_TYPES Debug Release)

include(cmake/cpm.cmake)
CPMUsePackageLock(package-lock.cmake)
include(cmake/mocks.cmake)
include(cmake/packages.cmake)
include(cmake/doxygen.cmake)
include(cmake/utils.cmake)
include(cmake/mocks.cmake)
include(cmake/tests.cmake)

if(NOT DEFINED ENV{CLION_IDE})
    enable_color_messages()
endif()

option(ENABLE_COVERAGE "Enable coverage reporting" OFF)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project(sat-firmware-launcher)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()

if(${CMAKE_C_COMPILER_VERSION} VERSION_LESS 11.2.1)
    message(FATAL_ERROR
        ${BOLD_RED}
        "Detected installation of gcc-arm-none-eabi it is too old to build "
        "the project, version 11.2.1 or newer is needed."
        ${NO_COLOR}
    )
endif()

include(GoogleTest)
include(CTest)
enable_testing()

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_subdirectory(libs)
add_subdirectory(firmware)
add_subdirectory(test)
