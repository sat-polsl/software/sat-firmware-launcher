image: registry.gitlab.com/sat-polsl/software/docker-arm-toolchain/toolchain

stages:
  - firmware
  - test
  - quality
  - docs

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - when: always

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  SONAR_OUTPUT_DIR: sonar-output

format:
  stage: quality
  script:
    - check_cpp_formatting

firmware:
  tags:
    - firmware
  stage: firmware
  interruptible: true
  cache:
    policy: pull-push
    key: "${CI_PIPELINE_ID}"
    paths:
      - build-firmware
      - sonar-firmware
  before_script:
    - cmake --preset ci/firmware
  script:
    - build-wrapper-linux-x86-64 --out-dir sonar-firmware cmake --build --preset ci/firmware

test-and-coverage:
  tags:
    - firmware
  stage: test
  interruptible: true
  cache:
    policy: pull-push
    key: "${CI_PIPELINE_ID}"
    paths:
      - build-firmware
      - build-tests
      - sonar-firmware
      - sonar-tests
  before_script:
    - cmake --preset ci/tests
  script:
    - build-wrapper-linux-x86-64 --out-dir sonar-tests cmake --build --preset ci/tests
    - ctest --test-dir build-tests --output-junit report.xml --output-on-failure
    - cmake --build --preset ci/coverage
  artifacts:
    when: always
    reports:
      junit: build-tests/report.xml
    paths:
      - build-tests/coverage.txt

sonarcloud-check:
  tags:
    - firmware
  stage: quality
  interruptible: true
  cache:
    policy: pull
    key: "${CI_PIPELINE_ID}"
    paths:
      - build-firmware
      - build-tests
      - sonar-firmware
      - sonar-tests
  before_script:
    - sed -i "/^#/d" sonar-firmware/build-wrapper-dump.json
    - sed -i "/^#/d" sonar-tests/build-wrapper-dump.json
    - mkdir ${SONAR_OUTPUT_DIR}
    - >
      jq -s '{version: .[0].version, captures: (.[0].captures + .[1].captures)}' 
      sonar-firmware/build-wrapper-dump.json 
      sonar-tests/build-wrapper-dump.json > ${SONAR_OUTPUT_DIR}/build-wrapper-dump.json
  script:
    - >
      sonar-scanner
      -Dsonar.host.url="${SONAR_HOST_URL}"
      -Dsonar.token="${SONAR_TOKEN}"
      -Dproject.settings=.sonar.properties

pages:
  tags:
    - firmware
  stage: docs
  interruptible: true
  cache:
    policy: pull
    key: "${CI_PIPELINE_ID}"
    paths:
      - build-firmware
  script:
    - cmake --build --preset ci/docs
    - mv build-firmware/docs/html public
  artifacts:
    paths:
      - public
  only:
    - main
